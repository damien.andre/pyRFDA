import sys
from PyQt4.QtGui import QApplication, QDialog, QMessageBox
from PySide.QtCore import QObject, SIGNAL, SLOT, Qt, QCoreApplication
from window import Ui_window

import pyaudio
import numpy as np
from matplotlib import pyplot as plt
import scipy.signal as signal
import time
import scipy.fftpack


########### INPUT SETTINGS ###########
RATE=44100        # <- capture frequency
TIME = 2.         # <- capture time in second
DELAY = 4         # <- delay before capturing
CHUNKSIZE = 1024  # <- buffer size
######################################


class Sample:
    def __init__(self):
        self.width = None
        self.length = None
        self.thickness = None
        self.mass = None
        self.xf = []
        self.yf = []
        self.mode = ''

    def set_value(self, attr, val):
        try:
            num = float(val)
            if (num <= 0.):
                setattr(self, attr, None)
                return 'Error : the ' + attr + ' value must be higher than 0'
            setattr(self, attr, num)
            return 'Setting ' + attr + ' to ' + val 
            return True
        except ValueError:
            setattr(self, attr, None)
            return 'Error : the ' + attr + ' value is invalid'


    def ok(self):
        return self.width is not None and self.length is not None and self.thickness is not None and self.mass is not None

    def compute_young_modulus(self, freq):
        b = self.width  
        t = self.thickness
        L = self.length
        m = self.mass
        fb = freq
        T = 1. + 6.585*(t/L)**2.
        E = 0.9465*(m*(fb**2)/b)*((L/t)**3.)*T
        return E

    def compute_shear_modulus(self, freq):
        b = self.width  
        t = self.thickness
        L = self.length
        m = self.mass
        ft = freq
        B = ( (b/t)+(t/b) ) / ( 4.*(t/b) - 2.52*((t/b)**2) + 0.21*((t/b)**6) )
        A = (0.5062 - 0.8776*(b/t) + 0.3504*((b/t)**2) - 0.0078*((b/t)**3)) / ( 12.03*(b/t) + 9.892*((b/t)**2))
        G = ( (4.*L*m*(ft**2)) / (b*t) ) * (B/(1+A))
        return G

    def save(self, name):
        store = JsonStore(name)
        store.put('sample', width=self.width, thickness = self.thickness,
                  length = self.length, mass = self.mass )

    def load_file(self, name):
        store = JsonStore(name)
        self.width = float(store.get('sample')['width'])
        self.thickness  = float(store.get('sample')['thickness'])
        self.length  = float(store.get('sample')['length'])
        self.mass  = float(store.get('sample')['mass'])




app = QApplication(sys.argv)
window = QDialog()
ui = Ui_window()
sample = Sample()

def status(msg):
    ui.label_status.setText(msg)
    ui.label_status.repaint()


def set_sample_value():
    sample.width = ui.doubleSpinBox_width.value()
    sample.thickness  = ui.doubleSpinBox_thickness.value()
    sample.length  = ui.doubleSpinBox_length.value()
    sample.mass  = ui.doubleSpinBox_mass.value()

def wait():
    for i in range(DELAY):
        status("going to record in " + str(DELAY - i - 1) + " s")
        time.sleep(1)
    status ("recording...")

def record():
    status ("ok")
    p = pyaudio.PyAudio()
    stream = p.open(format=pyaudio.paInt16, channels=1, rate=RATE, input=True, frames_per_buffer=CHUNKSIZE)

    frames = [] 
    for _ in range(0, int(RATE / CHUNKSIZE * TIME)):
        data = stream.read(CHUNKSIZE)
        frames.append(np.fromstring(data, dtype=np.int16))

    # convert the list of np-arrays into a 1D array (column-wise)
    result = np.hstack(frames)

    # close stream
    stream.stop_stream()
    stream.close()
    p.terminate()

    #### preparing data, separating the two channels
    time = np.array(range(0, len(result)))*1./RATE

    #### fast fourier transform of audio signal
    N = len(result)
    T = 1./RATE
    sample.yf = scipy.fftpack.fft(result)
    sample.xf = np.linspace(0.0, 1./T, N)

    num_val = (25000./RATE)*N
    sample.yf = sample.yf[0:num_val]
    sample.xf = sample.xf[0:num_val]
    sample.yf = N * np.abs(sample.yf)

    # select the frequence for max value 
    i = sample.yf.argmax(axis=0)
    sample.freq = sample.xf[i]

def onclick(event):
    # print('button=%d, x=%d, y=%d, xdata=%f, ydata=%f' %
    #       (event.button, event.x, event.y, event.xdata, event.ydata))
    if event.button == 3:
        sample.freq = event.xdata
    display_result()

def plot():
    #### plot the results 
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.plot(sample.xf, sample.yf)
    plt.xlabel('frequency (Hz)')
    plt.ylabel('amplitude DB')
    plt.title("Spectral analysis")
    fig.canvas.mpl_connect('button_press_event', onclick)
    plt.show()


def display_result():
    msg = 'For a natural frequency of {:.1f} Hz'.format(sample.freq)
    if sample.mode == 'bending':
        E = sample.compute_young_modulus(sample.freq)
        msg += '\nThe related Young\'s modulus is {:.2f} GPa'.format(E*1e-9)
    else: 
        G = sample.compute_shear_modulus(sample.freq)
        msg += '\nThe related shear modulus is {:.2f} GPa'.format(G*1e-9)
    status(msg + '\nYou can right click the plot to compute the result.')





def run_torsion():
    sample.mode = 'bending'
    set_sample_value()
    wait()
    record()
    display_result()
    plot()

def run_bending():
    sample.mode = 'torsion'
    set_sample_value()
    wait()
    record()
    display_result()
    plot()

    


ui.setupUi(window)
app.connect(ui.pushButton_quit, SIGNAL("clicked()"), app, SLOT("quit()"))
ui.pushButton_torsion.clicked.connect(run_torsion)
ui.pushButton_bending.clicked.connect(run_bending)


window.show()
sys.exit(app.exec_())



